package com.darkscientist.averyl.bungeegestion.bukkit.utils;

import com.darkscientist.averyl.bungeegestion.bukkit.BungeeGestionBukkit;
import org.bukkit.entity.Player;

public class VanishUtils {


    public void setVanish(Player p) {
        for(Player players : BungeeGestionBukkit.getInstance().getServer().getOnlinePlayers()) {
            p.hidePlayer(players);
        }
    }

    public void removeVanish(Player p) {
        for(Player players : BungeeGestionBukkit.getInstance().getServer().getOnlinePlayers()) {
            p.showPlayer(players);
        }
    }

}
