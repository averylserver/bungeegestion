package com.darkscientist.averyl.bungeegestion.bukkit.listeners;

import com.darkscientist.averyl.bungeegestion.bukkit.utils.VanishUtils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class MessengerListener implements PluginMessageListener {

    public VanishUtils vanishUtils = new VanishUtils();

    @Override
    public void onPluginMessageReceived(String s, Player player, byte[] bytes) {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));

        try {
            String subchannel = in.readUTF();
            if(subchannel.equals("SetVanish")) {
                vanishUtils.setVanish(player);
            }

            if(subchannel.equals("UnsetVanish")) {
                vanishUtils.removeVanish(player);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
