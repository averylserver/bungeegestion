package com.darkscientist.averyl.bungeegestion.bukkit;

import com.darkscientist.averyl.bungeegestion.bukkit.listeners.MessengerListener;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public class BungeeGestionBukkit extends JavaPlugin {


    @Getter
    public static BungeeGestionBukkit instance;

    @Override
    public void onEnable() {
        instance = this;
        getServer().getMessenger().registerIncomingPluginChannel(this, "averyl", new MessengerListener());
    }
}
