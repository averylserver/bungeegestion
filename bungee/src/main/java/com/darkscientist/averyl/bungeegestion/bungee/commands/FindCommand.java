package com.darkscientist.averyl.bungeegestion.bungee.commands;

import com.darkscientist.averyl.bungeegestion.bungee.BungeeGestionBungee;
import com.darkscientist.averyl.bungeegestion.bungee.utils.ConfigUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FindCommand extends Command {

    ConfigUtils configUtils = new ConfigUtils();

    public FindCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(sender.hasPermission(configUtils.getConfigString("permissions.find.find_player"))) {

            if(args.length == 0) {
                sender.sendMessage(configUtils.getFullConfigString("errors.find.err_no_args"));
            } else {
                ProxiedPlayer playerWanted = BungeeGestionBungee.getInstance().getProxy().getPlayer(args[0]);
                if(playerWanted == null) {
                    sender.sendMessage(configUtils.getFullConfigString("errors.find.err_not_found"));
                } else {
                    String serverName = playerWanted.getServer().getInfo().getName();
                    sender.sendMessage(configUtils.getFullConfigString("messages.find.find").replace("%player%", playerWanted.getDisplayName()).replace("%server%", serverName));
                }
            }

        } else {
            sender.sendMessage(configUtils.getFullConfigString("errors.find.err_no_perm"));
        }
    }
}
