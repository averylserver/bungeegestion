package com.darkscientist.averyl.bungeegestion.bungee.utils;

import com.darkscientist.averyl.bungeegestion.bungee.BungeeGestionBungee;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class DbUtils {

    private static Connection connection;
    private static Statement statement;

    public static void connect() {
        connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(BungeeGestionBungee.getInstance().getDataFolder(), "dbLite.db");
            if(!file.exists()) {
                file.createNewFile();
            }

            String url = "jdbc:sqlite:" + file.getPath();
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();
            onUpdate("CREATE TABLE IF NOT EXISTS vanish (\n"
                    + " uuid varchar(255) \n"
                    + ");");
            onUpdate("CREATE TABLE IF NOT EXISTS players (\n"
                    + " uuid varchar(255), \n"
                    + " firstco DATETIME NOT NULL DEFAULT (GETDATE()), \n"
                    + " lastco DATETIME NULL, \n"
                    + " timeco DATETIME NULL \n"
                    + ");");
            System.out.println("La base de données est chargée.");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        if(connection != null) {
            try {
                connection.close();
                System.out.println("La base de données est déconnectée.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void onUpdate(String str) {
        try {
            statement.execute(str);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet onQuery(String str) {

        try {
            return statement.executeQuery(str);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean hasAccount(String uuid) {

        try {
            PreparedStatement p = connection.prepareStatement("SELECT uuid FROM players WHERE uuid = ?");
            p.setString(1, uuid);
            ResultSet r = p.executeQuery();
            boolean hasAccount = r.next();
            p.close();
            return hasAccount;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    public static boolean isVanish(String uuid) {

        try {
            PreparedStatement p = connection.prepareStatement("SELECT uuid FROM vanish WHERE uuid = ?");
            p.setString(1, uuid);
            ResultSet r = p.executeQuery();
            boolean isVanish = r.next();
            p.close();
            return isVanish;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

}
