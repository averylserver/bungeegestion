package com.darkscientist.averyl.bungeegestion.bungee.commands;

import com.darkscientist.averyl.bungeegestion.bungee.utils.ConfigUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.DbUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.MessagingUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.Console;

public class VanishCommand extends Command {

    public ConfigUtils configUtils = new ConfigUtils();
    public MessagingUtils messagingUtils = new MessagingUtils();

    public VanishCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (args.length == 0) {
                if(player.hasPermission(configUtils.getConfigString("permissions.vanish.vanish_self"))) {
                    if(!DbUtils.isVanish(player.getUniqueId().toString())) {
                        messagingUtils.sendSetVanish(player, player.getServer().getInfo());
                    } else {
                        messagingUtils.sendUnsetVanish(player, player.getServer().getInfo());
                    }
                } else {
                    player.sendMessage(configUtils.getFullConfigString("error.vanish.err_no_perm"));
                }
            } else {
                if (player.hasPermission(configUtils.getConfigString("permissions.vanish.vanish_other"))) {

                } else {
                    player.sendMessage(configUtils.getFullConfigString("error.vanish.err_no_perm_other"));
                }
            }
        } else {
            return;
        }


    }
}
