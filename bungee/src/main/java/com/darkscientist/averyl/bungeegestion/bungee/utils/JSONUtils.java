package com.darkscientist.averyl.bungeegestion.bungee.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JSONUtils {


    private JsonObject object;

    public JSONUtils(JsonObject object) {
        this.object = object;
    }

    public JsonElement get(String path) {

        if(path == null)
            throw new IllegalArgumentException("Path cannot be null.");

        if(this.object == null)
            throw new IllegalArgumentException("Cannot find a valid JsonObject.");

        JsonObject current = this.object;

        String[] index = path.split("\\.");

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < index.length; i++) {

            String key = index[i];

            sb.append("key");

            if(!current.has(key))
                throw new NullPointerException(String.format("The element %s was not found at the path %s", key, path));

            JsonElement element = object.get(key);

            if(!element.isJsonObject()) {

                if(i + 1 == index.length) return element;
                else throw new NullPointerException(String.format("Cannot find a JsonObject at %s. Found: %s", sb.toString(), element.getClass().getName()));
            }
            current = element.getAsJsonObject();
            sb.append(".");
        }
        return null;
    }

    public String getString(String path) {
        return get(path).getAsString();
    }

    public int getInt(String path) {
        return get(path).getAsInt();
    }

    public double getDouble(String path) {
        return get(path).getAsDouble();
    }

    public long getLong(String path) {
        return get(path).getAsLong();
    }
}
