package com.darkscientist.averyl.bungeegestion.bungee.commands;

import com.darkscientist.averyl.bungeegestion.bungee.BungeeGestionBungee;
import com.darkscientist.averyl.bungeegestion.bungee.utils.ConfigUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.PlayerUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UUIDCommand extends Command {

    ConfigUtils configUtils = new ConfigUtils();
    PlayerUtils playerUtils = new PlayerUtils();

    public UUIDCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission(configUtils.getConfigString("permissions.uuid.find_uuid"))) {

            if(args.length == 0) {
                sender.sendMessage(configUtils.getFullConfigString("errors.uuid.err_no_args"));
            } else {
                ProxiedPlayer playerWanted = BungeeGestionBungee.getInstance().getProxy().getPlayer(args[0]);
                if(playerWanted == null) {
                    sender.sendMessage(configUtils.getFullConfigString("error.uuid.err_not_found"));
                } else {
                    sender.sendMessage(configUtils.getFullConfigString("messages.uuid.uuid").replace("%player%", playerWanted.getDisplayName().replace("%uuid%", playerUtils.getPlayerUUID(playerWanted))));
                }
            }
        } else {
            sender.sendMessage(configUtils.getFullConfigString("error.uuid.err_no_perm"));
        }
    }
}
