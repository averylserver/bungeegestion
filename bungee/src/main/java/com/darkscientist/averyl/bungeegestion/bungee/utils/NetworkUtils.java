package com.darkscientist.averyl.bungeegestion.bungee.utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Map;

public class NetworkUtils {

    public String url;
    private static HttpURLConnection con;

    public NetworkUtils(String url) {
        this.url = url;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }


    public Map<String, Calendar> getNameHistory(String uuid) throws IOException {
        JSONObject json = readJsonFromUrl("https://api.mojang.com/user/profiles/" + uuid + "/names");
        for(String s : json.keySet()) {

        }
        return null;
    }
}
