package com.darkscientist.averyl.bungeegestion.bungee.utils;

import com.darkscientist.averyl.bungeegestion.bungee.BungeeGestionBungee;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;

import java.util.Map;

public class ServersUtils {

    private ConfigUtils configUtils = new ConfigUtils();

    public Map<String, ServerInfo> servers = BungeeGestionBungee.getInstance().getProxy().getServers();

    public boolean checkServersPerms(String serverName, ProxiedPlayer player) {


        String permission_alone = configUtils.getConfigString("permissions.servers.permission_alone");

        if(player.hasPermission(permission_alone.replace("%server%", serverName))) {
            return true;
        }

        return false;
    }

    public boolean checkServerCommand(ProxiedPlayer player) {
        // On vérifie que le joueur a bien la permission d'effectuer la commande.
        String permission_command = configUtils.getConfigString("permissions.servers.permission_command");

        if(player.hasPermission(permission_command)) {
            return true;
        }

        return false;
    }

    public BaseComponent[] getServerMessageList(ProxiedPlayer player) {
        ComponentBuilder serverList = new ComponentBuilder( "" ).append( TextComponent.fromLegacyText( configUtils.getConfigString("messages.servers.list") ) );
        boolean first = true;
        for(ServerInfo server : servers.values()) {
            TextComponent serverTextComponent = new TextComponent( first ? configUtils.getConfigString("colors.servers.color_server_name") + server.getName() : configUtils.getConfigString("colors.servers.color_comma_list") + ", " + configUtils.getConfigString("colors.servers.color_server_name") + server.getName() );
            int count = server.getPlayers().size();
            serverTextComponent.setHoverEvent( new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder( configUtils.getConfigString("colors.servers.color_player_number") + count + ( count == 1 ? " "  + configUtils.getConfigString("general.single_player") : " " + configUtils.getConfigString("general.multiple_players") ) + "\n" ).append( configUtils.getConfigString("colors.servers.color_connect") + configUtils.getConfigString("messages.servers.click_connect") ).italic( true ).create() )
            );

            if(checkServersPerms(server.getName(), player)) {
                serverTextComponent.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/server " + server.getName() ) );
            }

            serverList.append( serverTextComponent );
            first = false;
        }

        return serverList.create();
    }

    public BaseComponent[] getConsoleServerMessageList() {
        ComponentBuilder serverList = new ComponentBuilder( "" ).append( TextComponent.fromLegacyText( configUtils.getConfigString("messages.servers.list") ) );
        boolean first = true;
        for(ServerInfo server : servers.values()) {
            int count = server.getPlayers().size();
            String serverNameWithCount = server.getName() + " (" + count + ( count == 1 ? " " + configUtils.getConfigString("general.single_player") : " " + configUtils.getConfigString("general.multiple_players")) + ")";
            TextComponent serverTextComponent = new TextComponent( first ? serverNameWithCount : ", " + serverNameWithCount );
            serverList.append( serverTextComponent );
            first = false;
        }

        return serverList.create();
    }

    public void sendPlayerOnServer(ProxiedPlayer player, ServerInfo server) {
        if(server == null) {
            player.sendMessage(configUtils.getErrorConfigString("errors.servers.err_serv_null"));
        } else if(!checkServersPerms(server.getName(), player)) {
            player.sendMessage(configUtils.getErrorConfigString("errors.servers.err_serv_perm"));
        } else {
            player.connect(server, ServerConnectEvent.Reason.COMMAND);
            player.sendMessage(configUtils.getFullConfigString("messages.servers.sent_to").replace("%server%", server.getName()));
        }



    }
}
