package com.darkscientist.averyl.bungeegestion.bungee;

import com.darkscientist.averyl.bungeegestion.bungee.commands.*;
import com.darkscientist.averyl.bungeegestion.bungee.utils.ConfigUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.DbUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.JSONUtils;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.nio.file.Files;

public class BungeeGestionBungee extends Plugin {

    @Getter
    public static BungeeGestionBungee instance;

    @Getter
    public Configuration config;


    @Getter
    public JsonObject object;

    ConfigUtils configUtils = new ConfigUtils();
    JSONUtils jsonUtils;

    @Override
    public void onEnable() {

        loadConfig();
        loadServerData();
        instance = this;
        System.out.println("Le plugin BungeeGestion pour BungeeCord est démarré !");
        getProxy().getPluginManager().registerCommand(this, new ServerCommand("server"));
        getProxy().getPluginManager().registerCommand(this, new IpCommand("ip"));
        getProxy().getPluginManager().registerCommand(this, new FindCommand("find"));
        getProxy().getPluginManager().registerCommand(this, new UUIDCommand("uuid"));
        getProxy().getPluginManager().registerCommand(this, new VanishCommand("vanish"));
        getProxy().registerChannel("averyl");
        DbUtils.connect();
        System.out.println();


    }

    private void loadConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdir();
                System.out.println("Dossier de configuration crée !");
            }

            File configFile = new File(getDataFolder(), "config.yml");
            File serversFile = new File(getDataFolder(), "servers.json");

            if (!configFile.exists()) {
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            }

            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

            if (!serversFile.exists()) {
                try (InputStream is_s = getResourceAsStream("servers.json");
                     OutputStream os_s = new FileOutputStream(serversFile)) {
                    ByteStreams.copy(is_s, os_s);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException("Impossible de charger config.yml", e);
        }
    }

    private void loadServerData() {

        InputStream stream = getResourceAsStream("servers.json");

        //Mets une NPE ici
        if(stream == null) return;

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        //A stocker en static ou de manière unique
        //Et j'utilise Gson 2.8.5
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        object = gson.fromJson(reader, JsonObject.class);

        //Et tu le stockes
        if (object != null) {
             this.jsonUtils = new JSONUtils(object);
        } else {
            System.out.println("servers.json est null");
        }

    }

    @Override
    public void onDisable() {
        DbUtils.disconnect();
    }
}
