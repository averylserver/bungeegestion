package com.darkscientist.averyl.bungeegestion.bungee.utils;

import com.darkscientist.averyl.bungeegestion.bungee.BungeeGestionBungee;

public class ConfigUtils {

    public String getConfigString(String str) {
        return BungeeGestionBungee.getInstance().getConfig().getString(str).replace("&", "§");
    }

    public String getPrefix() {
        return BungeeGestionBungee.getInstance().getConfig().getString("general.prefix");
    }

    public String getFullConfigString(String str) {
        return getPrefix() + getConfigString(str);
    }

    public String getErrorConfigString(String str) {
        return getPrefix() + getConfigString("errors.servers.error") + getConfigString(str);
    }
}
