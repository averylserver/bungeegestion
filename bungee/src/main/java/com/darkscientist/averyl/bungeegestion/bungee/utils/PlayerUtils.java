package com.darkscientist.averyl.bungeegestion.bungee.utils;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlayerUtils {

    public String getPlayerIP(ProxiedPlayer player) {
        return player.getAddress().toString();
    }

    public String getPlayerUUID(ProxiedPlayer player) {
        return player.getUniqueId().toString();
    }
}
