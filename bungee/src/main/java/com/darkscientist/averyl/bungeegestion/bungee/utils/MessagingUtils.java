package com.darkscientist.averyl.bungeegestion.bungee.utils;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class MessagingUtils {

    public void sendSetVanish(ProxiedPlayer player, ServerInfo serverInfo) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF("SetVanish");
            out.writeUTF(player.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        serverInfo.sendData("SetVanish", stream.toByteArray());

    }

    public void sendUnsetVanish(ProxiedPlayer player, ServerInfo serverInfo) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF("SetVanish");
            out.writeUTF(player.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        serverInfo.sendData("UnsetVanish", stream.toByteArray());

    }
}
