package com.darkscientist.averyl.bungeegestion.bungee.commands;

import com.darkscientist.averyl.bungeegestion.bungee.utils.ConfigUtils;
import com.darkscientist.averyl.bungeegestion.bungee.utils.ServersUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ServerCommand extends Command {


    private ServersUtils serversUtils = new ServersUtils();
    private ConfigUtils configUtils = new ConfigUtils();



    public ServerCommand(String name) {
        super(name);
        // Useless car la vérif de permission est faite maison
        // BungeeGestionBungee.getInstance().getConfig().getString("permissions.servers.permission_command")
    }

    @Override
    public void execute(CommandSender sender, String[] args) {


        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(serversUtils.checkServerCommand(player)) {
                // On continue le code et la commande
                if(args.length == 0) {
                    player.sendMessage(configUtils.getFullConfigString("messages.servers.current").replace("%current_server%", player.getServer().getInfo().getName()));

                    player.sendMessage(serversUtils.getServerMessageList(player));
                }

                if(args.length == 1) {
                    ServerInfo server = serversUtils.servers.get(args[0]);
                    serversUtils.sendPlayerOnServer(player, server);
                }

                return;
            } else {
                player.sendMessage(configUtils.getErrorConfigString("errors.servers.err_no_cmd_perm"));
            }
        } else {
            sender.sendMessage(serversUtils.getConsoleServerMessageList());
        }

    }
}
